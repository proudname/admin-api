FROM node:14
COPY . /web
WORKDIR /web
RUN npm install
RUN npm run-script build
RUN npm i -g serve
CMD serve -s build
