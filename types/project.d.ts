export declare global {
    type SuccessResponse<T> = { ok: 1 }&T;
    type FailResponse = {ok: 0, message: string};
    type DefaultExpressResponse<T> = SuccessResponse<T>|FailResponse;
    type ChartDataSet = {
        label: string,
        data: number[],
        backgroundColor: string[],
        borderWidth: 1
    }
    type TransactionDoc = {
        _id: string|number,
        data: {
            ids: number[],
            games: number[],
            items: any[]
        },
        createdAt: number,
        type: number,
        creator: string,
        state: number
    }
}
