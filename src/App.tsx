import React from 'react';
import './App.css';
import adapter from "./adapter";
import Dashboard from "./resources/dashboard/Dashboard";
import 'react-notifications/dist/react-notifications.css'
import { NotificationContainer } from 'react-notifications'
import axios from 'axios';
import config from "./config";
const { fetchUtils, Admin, Datagrid, List, TextField } = require('react-admin');

const httpClient = (url: string, options: any = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    const token = localStorage.getItem('authToken');
    options.headers.set('Authorization', `Bearer ${token}`);
    return fetchUtils.fetchJson(url, options);
};

const authProvider = {
    login: async ({username, password}: any) => {
        const {data} = await axios.post<{ok: 1, token: string}|{ok: 0, message: string}>('/api/auth/login', {
            token: config.apiSecret,
            username,
            password
        });
        if (!data.ok) return Promise.reject(data.message);
        localStorage.setItem('authToken', data.token)
        await new Promise(r => setTimeout(r, 1000));
    },
    checkAuth: async () => localStorage.getItem('authToken')
    ? Promise.resolve()
    : Promise.reject('Auth token is missing'),
    getPermissions: async () => localStorage.getItem('authToken')
        ? Promise.resolve()
        : Promise.reject(),
    logout: () => {
        localStorage.removeItem('authToken')
        return Promise.resolve();
    },
    checkError: (error: any) => {
        alert(error)
    },

};

const dataProvider = adapter('/api/v1', httpClient);

function App() {
  return (
      <Admin
          dataProvider={dataProvider}
          authProvider={authProvider}
          dashboard={Dashboard}
      >
          {/*<Resource name="bots" list={BotList} />*/}
        {/*<Resource name="bot_tasks" list={ListGuesser} />*/}
        <NotificationContainer/>
      </Admin>
  )
}

export const BotList = (props: any) => (
    <List {...props} title={'List of posts'}>
        <Datagrid>
            <TextField source="_id" />
            <TextField source="userName" />
        </Datagrid>
    </List>
);

export default App;
