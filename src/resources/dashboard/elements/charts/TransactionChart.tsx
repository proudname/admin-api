import React, {FunctionComponent, useEffect, useState} from "react";
import {Line} from "react-chartjs-2";
import Loader from "react-loading-overlay";
import axios from 'axios';
import Select from '@material-ui/core/Select';
import Option from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import { NotificationManager as N } from 'react-notifications'
import {  Moment } from 'moment';
import {FormControl, Grid, InputLabel, TextField} from "@material-ui/core";
import DateFnsUtils from '@date-io/moment';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    DatePicker
} from '@material-ui/pickers';
const moment = require('moment-timezone'),
    momentTZ = (date?: number): Moment => moment(date).tz('Europe/Moscow');

const TransactionChart: FunctionComponent<{ games: string[], system: number, type: number, project: string, title: string }> = ({ games, system, type, project, title }) => {

    const [loader, setLoader] = useState(true),
        [game, setGame] = useState(games[0]),
        [date, setDate] = useState<Moment | null>(momentTZ()),
        [options, setOptions] = useState<{ labels: string[], datasets: ChartDataSet[] }>({ labels: [], datasets: [] });

    const handleGame = async (value: string) => {
        setGame(value);
    }
    const handleDate = async (value: Moment | null) => {
        setDate(value);
    }

    const loadInfo = async () => {
        setLoader(true)
        try {
            const { data } = await axios.post<DefaultExpressResponse<{boundaries: number[], metrics: [{success: {_id: number|'uncategorized', items: TransactionDoc[]}[], fail: {_id: number|'uncategorized', items: TransactionDoc[]}[], all: {_id: number|'uncategorized', items: TransactionDoc[]}[] }]}>>(`/api/metrics/${project}`, {
                game,
                system,
                type,
                date: date?.format('YYYY-MM-DD')
            });

            if (!data.ok) throw new Error(data.message);
            const [ { success, fail, all} ] = data.metrics,
                labels: string[] = data.boundaries.map(bound =>  momentTZ(bound).format('HH')),
                successSet: ChartDataSet = {
                    label: 'Успешно',
                    data: data.boundaries.map(bound => {
                        const timeRangeDoc = success.find(failDoc => failDoc._id === bound);
                        if (!timeRangeDoc) return 0;
                        return timeRangeDoc.items.length;
                    }),
                    backgroundColor: ['rgba(129, 240, 86, 0.5)'],
                    borderWidth: 1
            },
                failSet: ChartDataSet = {
                    label: 'Неуспешно',
                    data: data.boundaries.map(bound => {
                        const timeRangeDoc = fail.find(failDoc => failDoc._id === bound);
                        if (!timeRangeDoc) return 0;
                        return timeRangeDoc.items.length;
                    }),
                    backgroundColor: ['rgba(240, 86, 86, 0.5)'],
                    borderWidth: 1
            },
                allSet: ChartDataSet = {
                    label: 'Все',
                    data: data.boundaries.map(bound => {
                        const timeRangeDoc = all.find(failDoc => failDoc._id === bound);
                        if (!timeRangeDoc) return 0;
                        return timeRangeDoc.items.length;
                    }),
                    backgroundColor: ['rgba(219, 227, 216, 0.5)'],
                    borderWidth: 1
            };

            await setOptions({labels, datasets: [successSet, failSet, allSet]});

        } catch (e) {
            N.error(e.message);
            if (/401/.test(e.message)) {
                localStorage.removeItem('authToken');
            }
        }

        setLoader(false);
    }

    useEffect(() => {
        const token = localStorage.getItem('authToken');
        axios.defaults.headers.common = {'Authorization': `Bearer ${token}`}
        loadInfo()
    }, [date, game])


    return <Loader active={loader} spinner>
        <Grid container>
            <Grid item xs={4} justify={'center'} alignItems={'center'}>
                <Typography component={'div'}>
                    <b><u>{title}</u></b>
                </Typography>
            </Grid>
            <Grid item xs={4} justify={'center'} alignItems={'center'}>
                <FormControl>
                    <InputLabel id="game-label">Игра</InputLabel>
                    <Select labelId="game-label" value={game} onChange={(e) => handleGame(e.target.value as string)}>
                        {games.map(gameName => <Option value={gameName}>{gameName}</Option>)}
                    </Select>
                </FormControl>
            </Grid>
            <Grid item xs={4} justify={'center'} alignItems={'center'}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        disableToolbar
                        disableFuture
                        variant="inline"
                        format="DD/MM/YYYY"
                        margin="normal"
                        label="Дата"
                        value={date}
                        autoOk={true}
                        onChange={handleDate}
                        KeyboardButtonProps={{
                            'aria-label': 'Изменить дату',
                        }}
                    />
                </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={12}>
                <Line data={options} />
            </Grid>
        </Grid>
    </Loader>
}

export default TransactionChart;
