import React, {FunctionComponent, useEffect, useState, ElementType} from "react";
import { Card, CardContent, CardActions } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';

const InfoBlock: FunctionComponent<{ title: string, infoData: string[]; actions: ElementType[] }> = ({ infoData, actions, title }) => {

        return (
          <Card>
            <CardContent>
              <Typography variant="h5" component="h2">
                {title}
              </Typography>
              {infoData.map(info => <Typography variant="body2" component="p">
                {info}
              </Typography>)}
            </CardContent>
            <CardActions>
                {actions}
              {/* <Button size="small">Learn More</Button> */}
            </CardActions>
          </Card>
        );
      }

export default InfoBlock;
