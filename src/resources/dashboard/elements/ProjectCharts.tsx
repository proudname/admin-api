import {Grid, TextField, Box} from "@material-ui/core";
import TransactionChart from "./charts/TransactionChart";
import React, {FunctionComponent, useState} from "react";
import InfoBlock from "./InfoBlock";


const ProjectCharts: FunctionComponent<{projectName: string}> = ({ projectName }) => {

    return <Grid container>
        <Box mt={5}>
            <Grid container xs={12} spacing={5}>
                <Grid item xs={12} md={6} lg={3}>
                    <InfoBlock title={'Последний отчет'} infoData={[`Дата: ${new Date()}`, `Дата: ${new Date()}`]} actions={[]}/>
                </Grid>
                <Grid item xs={12} md={6} lg={3}>
                    <InfoBlock title={'Последний отчет'} infoData={[`Дата: ${new Date()}`, `Дата: ${new Date()}`]} actions={[]}/>
                </Grid>
                <Grid item xs={12} md={6} lg={3}>
                    <InfoBlock title={'Последний отчет'} infoData={[`Дата: ${new Date()}`, `Дата: ${new Date()}`]} actions={[]}/>
                </Grid>
                <Grid item xs={12} md={6} lg={3}>
                    <InfoBlock title={'Последний отчет'} infoData={[`Дата: ${new Date()}`, `Дата: ${new Date()}`]} actions={[]}/>
                </Grid>
            </Grid>
        </Box>

        <Box mt={5}>
            <Grid container xs={12} spacing={5} >
                    <Grid item lg={6} xs={12}>
                        <TransactionChart type={1} games={['csgo']} system={1} project={projectName} title={'TM'}/>
                    </Grid>
                    <Grid item lg={6} xs={12}>
                        <TransactionChart type={1} games={['csgo']} system={2} project={projectName} title={'Shadowpay'}/>
                    </Grid>
                    <Grid item lg={6} xs={12}>
                        <TransactionChart type={1} games={['csgo', 'dota2', 'z1br', 'tf2', 'unturned', 'pd2', 'rust', 'kf2', 'bat1944', 'depth']} system={0} project={projectName} title={'Наши (отправка)'}/>
                    </Grid>
                    <Grid item lg={6} xs={12}>
                        <TransactionChart type={0} games={['csgo', 'dota2', 'z1br', 'tf2', 'unturned', 'pd2', 'rust', 'kf2', 'bat1944', 'depth']} system={0} project={projectName} title={'Наши (прием)'}/>
                    </Grid>
            </Grid>
        </Box>
    </Grid>
}

export default ProjectCharts;
