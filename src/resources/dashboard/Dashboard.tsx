import React, {FunctionComponent, useState} from 'react';
import {Grid, Tabs, Tab, AppBar} from '@material-ui/core';
import ProjectChart from "./elements/ProjectCharts";
import SwipeableView from 'react-swipeable-views';

const renderedList: Set<number> = new Set(),
    isNeedToRender = (active: boolean, index: number) => {
        if (active) {
            renderedList.add(index);
            return true;
        }
        return renderedList.has(index);
    }

const TabItem: FunctionComponent<{item: number, value: number}> = ({children, item, value}) => <div
    role="tabpanel"
    hidden={item !== value}
    id={`projects-tab-${item}`}
>
    { isNeedToRender(item === value, item) && children }
</div>


export default function() {

    const [tab, setTab] = useState(0);

    return <Grid container>
        <AppBar position="static" color="default" style={{zIndex: 1}}>
            <Tabs value={tab} onChange={(e, value) => setTab(value)} aria-label="simple tabs example" variant={'fullWidth'}>
                <Tab label="GOCS" aria-controls={'projects-tab-0'} />
                <Tab label="ANTISKINS" aria-controls={'projects-tab-1'} />
                <Tab label="SOMACASE" aria-controls={'projects-tab-2'}/>
                <Tab label="PROGAME" aria-controls={'projects-tab-3'}/>
            </Tabs>
        </AppBar>


        <SwipeableView
            index={tab}
            onChangeIndex={(tab) => setTab(tab)}
            ignoreNativeScroll={true}
            animateHeight={true}
        >
            <TabItem item={0} value={tab}>
                <ProjectChart projectName={'api2'}/>
            </TabItem>
            <TabItem item={1} value={tab}>
                <ProjectChart projectName={'api6'}/>
            </TabItem>
            <TabItem item={2} value={tab}>
                <ProjectChart projectName={'api5'}/>
            </TabItem>
            <TabItem item={3} value={tab}>
                <ProjectChart projectName={'api4'}/>
            </TabItem>
        </SwipeableView>
    </Grid>
}
